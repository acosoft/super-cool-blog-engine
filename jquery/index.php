<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <title></title>
    </head>
    <body>
		<input class="load" data-test="demo.php" type="submit" value="Load" />
		<input class="load" data-test="demo2.php" type="submit" value="Load" />
		
		<div id="prikaz" style="background-color: gray; color: white; padding: 20px;">
			
		</div>
		
		<script>
		
			jQuery(document).ready(function($) {
				$('.load').click(function() {
					var me = $(this);
					$.get(me.data('test'), function(data) {
						$('#prikaz').html(data);
					});
				});
			});
		
		</script>
    </body>
</html>

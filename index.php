<!DOCTYPE html>
<!--    Napisi nesto, ali ne isto sto i ja-->
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>

    <style>
      .kategorije 
      { float: right;  height: 500px; width: 200px; }

      .kategorije a
      { display: block; padding: 10px; background-color: yellowgreen;
	margin-bottom: 1px; color: white; text-decoration: none; }

      .kategorije a:hover
      { background-color: blue; color: white; }

      .kategorije-sadrzaja a
      { text-decoration: none; }
    </style>
  </head>
  <body>
    <?php
       include_once 'admin/Servisi.php';

       $servisi = new Servisi();
    ?>

    <div class="kategorije">
      <?php
	 $kategorije = $servisi->dajMiSveKategorije();
	 foreach ($kategorije as $kategorija)
	 {
	   $id = $kategorija->getId();
	   echo "<a href='kategorija.php?id=$id'>";
	   echo $kategorija->getNaziv();
	   echo '</a>';
	 }
      ?>
    </div>

    <?php
		$novosti = $servisi->dajMiNoviSadrzaj(10);

		foreach ($novosti as $sadrzaj)
		{
			echo '<h1>' . $sadrzaj->getNaslov() . '</h1>';
			echo '<div>' . $sadrzaj->getSadrzaj() . '</div>';
			
			$slika = 'slike/' . $sadrzaj->getId() . '.jpg';
			if(file_exists($slika))
			{
				echo "<img src='$slika' width='200px' />";
			}

			$kategorijeSadrzaja = $sadrzaj->getKategorije();
			if (count($kategorijeSadrzaja) > 0)
			{
				$linkovi = array();

				echo '<div class="kategorije-sadrzaja">';
				foreach ($kategorijeSadrzaja as $kategorija)
				{
					$id = $kategorija->getId();
					$naziv = $kategorija->getNaziv();

					$linkovi[] = "<a href='kategorija.php?id=$id'>$naziv</a>";
				}
				echo implode(', ', $linkovi);
				echo '</div>';
			}
		}
    ?>


  </body>
</html>

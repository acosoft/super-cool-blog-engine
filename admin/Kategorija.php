<?php

class Kategorija
{
	private $id;
	private $naziv;
	private $slug;
	
	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getNaziv()
	{
		return $this->naziv;
	}

	public function setNaziv($naziv)
	{
		$this->naziv = $naziv;
	}

	public function getSlug()
	{
		return $this->slug;
	}

	public function setSlug($slug)
	{
		$this->slug = $slug;
	}
}

?>

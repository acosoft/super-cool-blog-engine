<?php

include_once 'OsnovniSadrzaj.php';
include_once 'Sadrzaj.php';
include_once 'Kategorija.php';

session_start();

class Servisi
{
	const KORISNIK = 'korisnik';
	
	private function getConnection()
	{
		$con = new mysqli('localhost', 'root', NULL, 'p2blog');
		$con->set_charset('utf8');
		return $con;
	}
	
	private function pretvoriRedUKategoriju($red)
	{
		$kategorija = new Kategorija();
			
		$kategorija->setId($red['id']);
		$kategorija->setNaziv($red['naziv']);
		$kategorija->setSlug($red['slug']);
		
		return $kategorija;
	}
	
	public function dajMiSavSadrzaj()
	{
		$con = $this->getConnection();
		$data = array();
		
		$rezultat = $con->query('SELECT id, naslov FROM sadrzaji'); 
		/* @var $rezultat mysqli_result */
		
		while($red = $rezultat->fetch_array())
		{
			//imamo viška svojstava za koje nemamo vrijednosti
			//$sadrzaj = new Sadrzaj();
			
			$sadrzaj = new OsnovniSadrzaj();
			
			$sadrzaj->setId($red['id']);
			$sadrzaj->setNaslov($red['naslov']);
			
			$data[] = $sadrzaj;
		}
		
		$con->close();
		return $data;		
	}
	
	public function dajMiSveKategorije()
	{
		$con = $this->getConnection();
		
		$data = array();
		
		$rezultat = $con->query('SELECT * FROM kategorije'); 
		/* @var $rezultat mysqli_result */
		
		while($red = $rezultat->fetch_array())		
		{
			//uvodimo novu privatnu metodu, da nam se kod ne ponavlja
//			$kategorija = new Kategorija();
//			
//			$kategorija->setId($red['id']);
//			$kategorija->setNaziv($red['naziv']);
//			$kategorija->setSlug($red['slug']);
			
			//$data[] = $kategorija;
			$data[] = $this->pretvoriRedUKategoriju($red);
		}
		
		$con->close();
		return $data;
	}
	
	/**
	 * 
	 * @param mysqli $con
	 * @param Sadrzaj $sadrzaj
	 */
	private function spremiKategorije($con, $sadrzaj)
	{
		$id = $sadrzaj->getId();
		$kategorije = $sadrzaj->getKategorije();
		
		$stm = $con->prepare('INSERT INTO pripada(id_sadrzaja, id_kategorije)
			VALUES(?, ?)');
		
		foreach ($kategorije as $kategorija) /* @var $kategorija Kategorija */
		{
			$idKategorije = $kategorija->getId();
			$stm->bind_param('ii', $id, $idKategorije);
			$stm->execute();
		}
	}
	
	/**
	 * 
	 * @param Sadrzaj $sadrzaj
	 */
	public function objaviSadrzaj($sadrzaj)
	{
		$con = $this->getConnection();
		
		$sadrzajSadrzaja = $sadrzaj->getSadrzaj();
		$naslov = $sadrzaj->getNaslov();
		$datum = $sadrzaj->getDatum()->format('Y-m-d H:i:s');
		$korisnik = $sadrzaj->getKorisnik();
		$slug = uniqid();
		
//		$con->query("INSERT INTO sadrzaji(naslov, sadrzaj, id_korisnika, datum, slug) 
//			VALUES ('$naslov', '$sadrzajSadrzaja', '$korisnik', '$datum', '$slug')");
		
		$stm = $con->prepare('INSERT INTO sadrzaji(naslov, sadrzaj, id_korisnika, datum, slug) VALUES(?, ?, ?, ?, ?)');
				
		$stm->bind_param('ssiss', $naslov, $sadrzajSadrzaja, $korisnik, $datum, $slug);
		$stm->execute();
		
		$posljednjiId = $stm->insert_id;
		$sadrzaj->setId($posljednjiId);
		
		$this->spremiKategorije($con, $sadrzaj);
		
		if($con->errno)
		{
			//nakon što bacimo grešku, kod nakon toga se više ne izvršava
			//zato pokušavamo zatvoriti conection prije bacanja greske
			$con->close();
			throw new Exception($con->error);
		}
		
		$con->close();
	}
	
	
	/**
	 * 
	 * @param Sadrzaj $sadrzaj
	 */
	public function urediSadrzaj($sadrzaj)
	{
	    $con = $this->getConnection();

		$stm = $con->prepare('UPDATE sadrzaji SET naslov = ?, sadrzaj = ? WHERE id = ?');
		$naslov = $sadrzaj->getNaslov();
		$sadrzajSadrzaja = $sadrzaj->getSadrzaj();
		$id = $sadrzaj->getId();
		
		$stm->bind_param('ssi', 
				$naslov, 
				$sadrzajSadrzaja,
				$id);
		
		$stm->execute();
		
	    $con->close();
	}
	
	public function izbrisiSadrzaj($id)
	{
		$con = $this->getConnection();

		//$con->query("DELETE FROM sadrzaji WHERE id = $id");
		
		$cmd = $con->prepare("DELETE FROM sadrzaji WHERE id = ?");
		$cmd->bind_param('i', $id);
		$cmd->execute();
		
		$con->close();
		
	}
	
	public function dajMiSadrzaj($id)
	{
		$con = $this->getConnection();
		$stm = $con->prepare("SELECT naslov, sadrzaj FROM sadrzaji WHERE id = ?");
		$stm->bind_param('i', $id);
		
		if($stm->execute())
		{
			//$rezultat = $stm->get_result();
			$stm->bind_result($naslov, $sadrzajSadrzaja);
			if($stm->fetch())
			{
				//$red = $rezultat->fetch_array();

				$sadzaj = new Sadrzaj();
				$sadzaj->setId($id);
				//$sadzaj->setNaslov($red['naslov']);
				//$sadzaj->setSadrzaj($red['sadrzaj']);
				$sadzaj->setNaslov($naslov);
				$sadzaj->setSadrzaj($sadrzajSadrzaja);
			}
		}
		
		$con->close();
		
		return $sadzaj;
	}
	
	public function getKategorijeSadrzaja($idSadrzaja)
	{
		$con = $this->getConnection();
		
//		$rezultat = $con->query("SELECT k.id, k.naziv, k.slug
//			FROM pripada AS p, kategorije AS k
//			WHERE p.id_kategorije = k.id AND  p.id_sadrzaja = $idSadrzaja");
		
		$stm = $con->prepare('SELECT k.id, k.naziv, k.slug
			FROM pripada AS p, kategorije AS k
			WHERE p.id_kategorije = k.id AND p.id_sadrzaja = ?');		 $stm->bind_param('i', $idSadrzaja);
		$stm->execute();
		
		$rezultat = $stm->get_result();
		$data = array();
		
		/* @var $rezultat mysqli_result */
		while($red = $rezultat->fetch_array())
		{
//			$kategorija = new Kategorija();
//			
//			$kategorija->setId($red['id']);
//			$kategorija->setNaziv($red['naziv']);
//			$kategorija->setSlug($red['slug']);
//			
//			$data[] = $kategorija;
			
			$data[] = $this->pretvoriRedUKategoriju($red);
		}
		
		$con->close();
		return $data;
	}
	
	public function dajMiNoviSadrzaj($broj)
	{
		$con = $this->getConnection();
		
		$rezultat = $con->query("select nadimak, naslov, sadrzaj, datum, sadrzaji.id
			from sadrzaji, korisnici
			where korisnici.id = sadrzaji.id_korisnika
			order by datum desc
			limit 0, $broj");
		
		$data = array();
		
		/* @var $rezultat mysqli_result */
		while($red = $rezultat->fetch_array())
		{
			$sadrzaj = new Sadrzaj();
			
			$sadrzaj->setId($red['id']);
			$sadrzaj->setKorisnik($red['nadimak']);
			$sadrzaj->setDatum($red['datum']);
			$sadrzaj->setNaslov($red['naslov']);
			$sadrzaj->setSadrzaj($red['sadrzaj']);
			
			$kategorije = $this->getKategorijeSadrzaja($red['id']);
			$sadrzaj->setKategorije($kategorije);
			
			$data[] = $sadrzaj;
		}
		
		$con->close();
		return $data;
	}

	public function login($username, $password)
	{
		//provjera na bazi podataka
		$con = $this->getConnection();
		$rezultat = $con->query("SELECT id, nadimak, zaporka 
			FROM korisnici 
			WHERE email = '$username' OR nadimak = '$username'");
		
		/* @var $rezultat mysqli_result */
		$red = $rezultat->fetch_array();
		
		if($red && $red['zaporka'] == $password)
		{
			//kada je provjera na bazi prošla, onda ...
			$_SESSION[self::KORISNIK] = $red['nadimak'];
			$_SESSION['korisnik_id'] = $red['id'];
			return true;
		}
		else
		{
			$this->logout();
			return false;
		}
	}
	
	public function getLogiraniKorisnikId()
	{
		return $_SESSION['korisnik_id'];
	}
	
	public function logout()
	{
		unset($_SESSION[self::KORISNIK]);
	}
	
	public function isLogiran()
	{
		$test = isset($_SESSION[self::KORISNIK]);
		return $test;
	}
	
	public function getLogiraniKorisnik()
	{
		return $_SESSION[self::KORISNIK];
	}
}

?>

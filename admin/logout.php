<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
		<?php
			include './Servisi.php';
		
			$servisi = new Servisi();
			$servisi->logout();
		?>
		
		<h1>Uspješno ste odjavljeni!</h1>
		<a href="login.php">Prijavi se</a>
    </body>
</html>

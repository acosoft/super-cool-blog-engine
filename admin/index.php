<!DOCTYPE html>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php
       include_once './Servisi.php';
       $servisi = new Servisi();

       if ($servisi->isLogiran())
       {
	 include './izbornik.php';

	 echo '<table>';

	 $sve = $servisi->dajMiSavSadrzaj();
	 foreach ($sve as $sadrzaj)
	 {
	   echo '<tr>';
	   echo '<td>' . $sadrzaj->getNaslov() . '</td>';
	   echo '<td><a href="potvrda-brisanja.php?id=' . $sadrzaj->getId() . '">izbriši</a></td>';
	   echo '<td><a href="izmjena.php?id=' . $sadrzaj->getId() . '">izmijeni</a></td>';
	   echo '</tr>';
	 }

	 echo '</table>';
       }
       else
       {
	 ?>

         <h1>Upozorenje!</h1>
         <div>Morate biti prijavljeni da biste pristupili ovoj stranici ... 
           <a href="login.php">Prijava</a></div>

	 <?php
       }
    ?>
  </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
		<link rel="stylesheet" href="style.css" />
    </head>
    <body>
		<?php
			include_once './Servisi.php';
			$servisi = new Servisi();
			$poruka = "";
			
			if($servisi->isLogiran())
			{
				include './izbornik.php';
				$id = $_REQUEST['id'];
				$sadrzaj = $servisi->dajMiSadrzaj($id);
				
				if(isset($_REQUEST['akcija']))
				{
					$sadrzaj = new Sadrzaj();
					
					$sadrzaj->setId($id);
					$sadrzaj->setNaslov($_REQUEST['naslov']);
					$sadrzaj->setSadrzaj($_REQUEST['sadrzaj']);
					
					try
					{
						$servisi->urediSadrzaj($sadrzaj);
						$poruka = "Sadrzaj je uspješno izmjenjen";
					}
					catch (Exception $exc)
					{
						echo $exc->getTraceAsString();
					}
				}
				
				?>
		
	<div style="float: left;">
		<?php if($poruka) echo "<h1>$poruka</h1>"; ?>
		<form method="POST" enctype="multipart/form-data">
			<div>	
				<div>Naslov</div>
				<input type="text" name="naslov" value="<?php echo $sadrzaj->getNaslov(); ?>" />
			</div>	

			<div>
				<div>Sadržaj</div>
				<textarea name="sadrzaj" rows="10" cols="50"><?php echo $sadrzaj->getSadrzaj(); ?></textarea>
			</div>

			<input type="submit" name="akcija" value="Spremi" />
		</form>				
	</div>		
		
				<?php
			}
			else
			{
				echo '<h1>Upozorenje!</h1>
				<div>Morate biti prijavljeni da biste pristupili ovoj stranici ... 
					<a href="login.php">Prijava</a></div>';
			}
		?>
    </body>
</html>

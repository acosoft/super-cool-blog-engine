<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
		<?php 
//			session_start();
//			$_SESSION['test'] = 'super test : )';
		
			include_once './Servisi.php';
			$servisi = new Servisi();
			
			if(isset($_REQUEST['korisnik']))
			{
				$korisnik = $_REQUEST['korisnik'];
				$zaporka = $_REQUEST['zaporka'];
				
				//provjera na bazi da li su uneseni podaci točni
				if($servisi->login($korisnik, $zaporka) == true)
				{
					//ako su uneseni podaci točni, zapamti korisničko ime prijavljenog korisnika
					?>
					<h1>Uspješno ste prijavljeni ... <a href="index.php">Nastavi</a></h1>
					<?php
				}
				else
				{
					?>
					<h1>Neuspješna prijava ...</h1>
					<?php
				}
			}
		?>

		<?php if($servisi->isLogiran() == false) : ?>
		<form method="POST" enctype="multipart/form-data">
			<div><input type="text" name="korisnik" value="" placeholder="Upišite korisničko ime" /></div>
			<div><input type="password" name="zaporka" value="" placeholder="Upišite vašu zaporku" /></div>
			<div><input type="submit" value="Prijava" /></div>
		</form>
		<?php else: ?>
			Trenutno ste prijavljeni kao <b><?php echo $servisi->getLogiraniKorisnik(); ?></b> ... 
			<a href="logout.php">Logout</a>
		<?php endif; ?>
    </body>
</html>

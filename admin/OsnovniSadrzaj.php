<?php

class OsnovniSadrzaj
{
	private $id;
	private $naslov;
	
	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getNaslov()
	{
		return $this->naslov;
	}

	public function setNaslov($naslov)
	{
		$this->naslov = $naslov;
	}
}

?>

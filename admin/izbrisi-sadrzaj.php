<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
		<link rel="stylesheet" href="style.css" />
    </head>
    <body>
		<?php
			include_once './Servisi.php';
			$servisi = new Servisi();
			
			if($servisi->isLogiran())
			{
				include './izbornik.php';
				
				$id = $_REQUEST['id'];
				$servisi->izbrisiSadrzaj($id);
				
				echo '<h1>Sadržaj je uspješno izbrisan</h1>';
				echo '<a href="index.php">Index</a>';
			}
			else
			{
				?>
		
				<h1>Upozorenje!</h1>
				<div>Morate biti prijavljeni da biste pristupili ovoj stranici ... 
					<a href="login.php">Prijava</a></div>
		
				<?php
			}
		?>
    </body>
</html>

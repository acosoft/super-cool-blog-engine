<?php

class Sadrzaj 
	extends OsnovniSadrzaj
{
	private $sadrzaj;
	private $datum;
	private $korisnik;
	private $kategorije;
	
	public function getKategorije()
	{
		return $this->kategorije;
	}

	public function setKategorije($kategorije)
	{
		$this->kategorije = $kategorije;
	}

	public function getSadrzaj()
	{
		return $this->sadrzaj;
	}

	public function setSadrzaj($sadrzaj)
	{
		$this->sadrzaj = $sadrzaj;
	}

	public function getDatum()
	{
		return $this->datum;
	}

	public function setDatum($datum)
	{
		$this->datum = $datum;
	}

	public function getKorisnik()
	{
		return $this->korisnik;
	}

	public function setKorisnik($korisnik)
	{
		$this->korisnik = $korisnik;
	}
}

?>

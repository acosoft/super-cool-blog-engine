<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
		<link rel="stylesheet" href="style.css" />
    </head>
    <body>
		<?php
			include_once './Servisi.php';
			$servisi = new Servisi();
			
			if($servisi->isLogiran())
			{
				include './izbornik.php';
				
				if(isset($_REQUEST['akcija']))
				{
					$sadrzaj = new Sadrzaj();
					
					$sadrzaj->setNaslov($_REQUEST['naslov']);
					$sadrzaj->setSadrzaj($_REQUEST['sadrzaj']);
					$kategorije = $_REQUEST['kat'];	// u htmlu, ovo polje se zove kat[]
					
					$kategorijeSadrzaja = array();
					foreach ($kategorije as $idKategorije)
					{
						$temp = new Kategorija();
						$temp->setId($idKategorije);
						$kategorijeSadrzaja[] = $temp;
						//nismo postavili sve vrijednosti koje mozemo postaviti
					}
					
					$sadrzaj->setKategorije($kategorijeSadrzaja);
					
					$korisnik = $servisi->getLogiraniKorisnikId();
					$sadrzaj->setKorisnik($korisnik);
					
					$sadrzaj->setDatum(new DateTime());					
					
					try
					{
						$servisi->objaviSadrzaj($sadrzaj);
						
						if($_FILES['slika'])
						{
							$id = $sadrzaj->getId();
							
							$slika = $_FILES['slika'];
							$filename = $id . '.jpg';

							move_uploaded_file($slika['tmp_name'], '../slike/' . $filename);
						}
					}
					catch (Exception $exc)
					{
						echo $exc->getTraceAsString();
					}
				}
				
				?>
		
	<div style="float: left;">
		<form method="POST" enctype="multipart/form-data">
			<div>	
				<div>Naslov</div>
				<input type="text" name="naslov" value="" />
			</div>	

			<div>
				<div>Sadržaj</div>
				<textarea name="sadrzaj" rows="10" cols="50"></textarea>
			</div>
			
			<h3>Kategorije</h3>
			<?php 
				
				$kategorije = $servisi->dajMiSveKategorije();
				foreach ($kategorije as $kategorija)
				{
					echo '<div><input name="kat[]" value="' . $kategorija->getId() . '" type="checkbox" />' . $kategorija->getNaziv() . '</div>';
				}
			
			?>

			<div class="odabir-datoteke">
				<input type="file" name="slika" value="" />
			</div>
			<input type="submit" name="akcija" value="Spremi" />
		</form>				
	</div>		
		
				<?php
			}
			else
			{
				echo '<h1>Upozorenje!</h1>
				<div>Morate biti prijavljeni da biste pristupili ovoj stranici ... 
					<a href="login.php">Prijava</a></div>';
			}
		?>
    </body>
</html>
